angular.module('PGapp.controllers', [])

.controller('SplashCtrl', function($scope, WSService, $rootScope, localStorageService, $timeout, PGCons, $ionicPopup, $ionicLoading, $q, $state, $ionicScrollDelegate) {

    /*$scope.ready = function() {
        var deferred = $q.defer();
        ionic.DomUtil.ready(function() {
            deferred.resolve();
        });
        return deferred.promise;
    }*/
    var deferred = $q.defer();
    $timeout(function() {
        deferred.resolve();
    }, 1e4);
    $scope.resolve = false;
    var myAsyncTasks = [];
    myAsyncTasks.push(WSService.getLoadAppli(deferred));
    //myAsyncTasks.push($scope.ready());

    $q.all(myAsyncTasks).then(function(results) {
        $scope.resolve = true;
        $rootScope.menuList = results[0].data.pages;
        //Ajout du prochainement
        results[0].data.listes.unshift(PGCons.proch);
        $rootScope.selList = results[0].data.listes;
        $rootScope.bannieres = results[0].data.bannieres;
        $scope.contentsplash = results[0].data.contentsplash;
        $rootScope.saisonouverte = results[0].data.saisonouverte;
        if (PGCons.version !== results[0].data.version) {
            $ionicPopup.alert({
                title: 'Mise à jour',
                okType: 'button-balanced',
                template: 'Une nouvelle version est disponible, nous vous conseillons de la télécharger dès que possible.'
            });
        }
        if (results[0].data.contentsplash === '') {
            //$ionicLoading.hide();
            $state.go('app.liste');
        } else {
            $timeout(function() {
                $ionicScrollDelegate.resize();
            }, 150);
            $timeout(function() {
                $ionicLoading.hide();
            }, 500);
        }
    }, function(results) {
        //Pas sur que ce code soit atteignable
        //if (results.status === 0) {
        $ionicLoading.hide();
        $state.go('app.timeout');
        //}
    });

    var isRegitered = localStorageService.get("isRegitered") || false;
    if (!isRegitered && window.device.uuid) {
        //alert(window.device.uuid);
        WSService.registerUser(window.device.uuid).success(function(data) {
            localStorageService.set("isRegitered", true);
        });
    }
})

.controller('AppCtrl', function($scope, $q, $sce, $rootScope, $state, $location, WSService, $ionicPopup, $ionicModal, PGCons, $timeout, localStorageService, $ionicScrollDelegate, $ionicSlideBoxDelegate, $ionicLoading) {
    // Form data for the login modal
    $scope.loginData = {};
    $scope.resolve = false;
    $scope.showSearch = false;
    $scope.displayFooter = false;
    $scope.nextState = "";
    $scope.isLogin = localStorageService.get("login") || false;
    $scope.interval = PGCons.interval;

    $scope.menuList = $rootScope.menuList;
    $scope.bannieres = $rootScope.bannieres;
    $scope.selList = $rootScope.selList;
    $scope.saisonouverte = $rootScope.saisonouverte;

    $scope.ready = function() {
        var deferred = $q.defer();
        ionic.DomUtil.ready(function() {
            deferred.resolve();
        });
        return deferred.promise;
    };
    var myAsyncTasks = [];
    if (!$scope.menuList) {
        console.log('Chargement menu car inconnu');
        var deferred = $q.defer();
        $timeout(function() {
            deferred.resolve();
        }, 1e4);
        myAsyncTasks.push(WSService.getLoadAppli(deferred));
        myAsyncTasks.push($scope.ready());

        $q.all(myAsyncTasks).then(function(results) {
            $scope.menuList = results[0].data.pages;
            var proch = {
                lib: "Prochainement",
                version: "0.0.0.1",
                clef: ""
            };
            results[0].data.listes.unshift(PGCons.proch);
            $scope.selList = results[0].data.listes;
            $scope.bannieres = results[0].data.bannieres;
            $scope.saisonouverte = results[0].data.saisonouverte;
            //$scope.contentsplash = results[0].data.contentsplash;
            $timeout(function() {
                $ionicSlideBoxDelegate.$getByHandle('sliderBan').update();
            }, 500);
        }, function(results) {
            //Pas sur que ce code soit atteignable
            //if (results.status === 0) {
            $state.go('app.timeout');
            //}
        });
    }
    //Premier accès
    if (localStorageService.get("saveInCal") === null) {
        localStorageService.set("saveInCal", true);
    }

    var successHandler = function(result) {};
    var errorHandler = function(result) {};
    $scope.trackPage = function(pageName) {
        if (gaPlugin !== null) {
            gaPlugin.trackPage(successHandler, errorHandler, pageName);
        } else {
            console.log('trackPage : ' + pageName);
        }
    };

    $scope.addToSelection = function(id) {
        var mySelection = localStorageService.get("mySelection");
        if (!mySelection) {
            mySelection = [];
        }
        if (!mySelection[id] || false) {
            var confirmPopup = $ionicPopup.confirm({
                title: 'Ma sélection',
                template: 'Voulez-vous ajouter ce spectacle à votre sélection ?',
                cancelText: 'Annuler',
                okType: 'button-balanced'
            });
            confirmPopup.then(function(res) {
                if (res) {
                    mySelection[id] = true;
                    localStorageService.set("mySelection", mySelection);
                }
            });
        } else {
            var alertPopup = $ionicPopup.alert({
                title: 'Ma sélection',
                template: 'Ce spectacle est déjà dans votre sélection',
                okType: 'button-balanced'
            });
        }
    };


    $scope.slideHasChanged = function(index) {
        $scope.slideIndex = index;
        if (($ionicSlideBoxDelegate.$getByHandle('sliderBan').count() - 1) == index) {
            $timeout(function() {
                $ionicSlideBoxDelegate.$getByHandle('sliderBan').slide(0);
            }, $scope.interval);
        }
    };

    $scope.updateLogin = function(param) {
        $scope.isLogin = param;
        if (!param) {
            localStorageService.remove("login");
            localStorageService.remove("mdp");
            localStorageService.remove("idutil");
            $location.url('/app/liste/');
        }
    };

    $scope.goInApp = function(url) {
        var ref = window.open(url, '_blank', 'location=no,closebuttoncaption=Quitter la réservation');
    };
    $scope.goOutApp = function(url) {
        var ref = window.open(url, '_system');
    };

    $scope.showWaiting = function(inc) {
        $ionicLoading.show({
            template: '<ion-spinner icon="android" class="spinner-balanced"></ion-spinner><br>Chargement...'
        });
    };
    $scope.hideWaiting = function() {
        $ionicLoading.hide();
    };

    $scope.openFiche = function(idf, modal) {
        $scope.hideFooter();
        $scope.trackPage('openFiche');
        $scope.showWaiting();
        $scope.idf = idf;
        $scope.displayVideo = false;
        //Déclaration de la modal video à chaque ouverture de fiche
        //Suppression à chaque changement de vue
        $ionicModal.fromTemplateUrl('templates/video_modal.html', {
            scope: $scope
        }).then(function(modal) {
            $scope.videoModal = modal;
        });
        $ionicModal.fromTemplateUrl('templates/fiche.html', {
            scope: $scope
        }).then(function(modal) {
            $scope.fiche = modal;
        });

        $scope.nextSlide = function() {
            $ionicSlideBoxDelegate.$getByHandle('sliderRecom').next();
        };
        $scope.previousSlide = function() {
            $ionicSlideBoxDelegate.$getByHandle('sliderRecom').previous();
        };

        var deferred = $q.defer();
        $timeout(function() {
            deferred.resolve();
        }, 1e4);
        var myAsyncTasks = [];
        myAsyncTasks.push(WSService.getSpectacle(deferred, idf));

        $scope.trustSrc = function(src) {
            return $sce.trustAsResourceUrl(src);
        };
        $q.all(myAsyncTasks).then(function(results) {
            $scope.WSFiche = results[0].data.fiche;
            if ($scope.WSFiche.listvideo.length > 0) {
                $scope.displayVideo = $scope.WSFiche.listvideo[0].type === 'YOU' || $scope.WSFiche.listvideo[0].type === 'VIM';
                $scope.WSFiche.listvideo[0].urlVim = "https://player.vimeo.com/video/" + $scope.WSFiche.listvideo[0].clef;
            }

            $timeout(function() {
                $ionicSlideBoxDelegate.$getByHandle('sliderFiche').update();
            }, 700);
            if ($scope.WSFiche.listrecom.length > 0) {
                $timeout(function() {
                    $ionicSlideBoxDelegate.$getByHandle('sliderRecom').update();
                }, 800);
            }
            //Demi second pour mettre à jour le slide (au pire mettre le hide dans le timeout)
            $scope.hideWaiting();
            if (modal) {
                $scope.fiche.show();
                $scope.scrollTopModal();
            } else {
                //modal = false mais je suis une popup deja ouverte (cas de clic sur recom from popup)
                if ($scope.fiche.isShown()) {
                    $scope.scrollTopModal();
                }
                $scope.scrollTop();
            }
        }, function(results) {
            //if (results.status === 0) {
            $scope.hideWaiting();
            $state.go('app.timeout');
            //}
        });
    };

    $scope.closeFiche = function() {
        /*$ionicSlideBoxDelegate.$getByHandle('sliderFiche').slide(0);
        if ($scope.WSFiche.listrecom.length > 0) {
            $ionicSlideBoxDelegate.$getByHandle('sliderRecom').slide(0);
        }*/
        $scope.fiche.hide();
        $scope.fiche.remove();
        $scope.checkScroll();
    };
    $scope.closeVideoModal = function() {
        $scope.videoModal.hide();
        $scope.videoModal.remove();
        $ionicModal.fromTemplateUrl('templates/video_modal.html', {
            scope: $scope
        }).then(function(modal) {
            $scope.videoModal = modal;
        });

    };

    $scope.goToResult = function(txtSearch) {
        if (typeof cordova !== "undefined") {
            cordova.plugins.Keyboard.close();
        }
        $scope.trackPage('searchPage');
        $rootScope.txtSearch = txtSearch;
        console.log($rootScope.txtSearch);
        $scope.closeSearch();
        $state.go('app.liste', {
            'mode': 'search'
        }, {
            'reload': true
        });
    };
    $scope.toogleSearch = function() {
        $scope.showSearch = !$scope.showSearch;
        if ($scope.showSearch && typeof cordova !== "undefined") {
            cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
        } else if (!$scope.showSearch && typeof cordova !== "undefined") {
            cordova.plugins.Keyboard.hideKeyboardAccessoryBar(false);
        }
    };
    //necessaire pour le clic sur le burger
    $scope.closeSearch = function() {
        if ($scope.showSearch) {
            $scope.toogleSearch();
        }
    };

    $scope.hideFooter = function() {
        $scope.displayFooter = false;
    };

    $scope.scrollTop = function() {
        console.log('scrollTop');
        $ionicScrollDelegate.$getByHandle('scroller').scrollTop(true);
    };
    $scope.scrollTopModal = function() {
        console.log('scrollTopModal');
        $ionicScrollDelegate.$getByHandle('scrollerModal').scrollTop(true);
    };

    $scope.checkScroll = function() {
        var currentTop = $ionicScrollDelegate.$getByHandle('scroller').getScrollPosition().top;
        if (currentTop > 0) {
            $scope.displayFooter = true;
        } else {
            $scope.displayFooter = false;
        }
        //le if prévient le $digest already in progress
        if (!$scope.$$phase) {
            $scope.$apply();
        }
    };
    $scope.checkScrollModal = function() {
        var currentTop = $ionicScrollDelegate.$getByHandle('scrollerModal').getScrollPosition().top;
        if (currentTop > 0) {
            $scope.displayFooterModal = true;
        } else {
            $scope.displayFooterModal = false;
        }
        if (!$scope.$$phase) {
            $scope.$apply();
        }
    };

    $scope.openIconLeg = function() {
        $ionicPopup.alert({
            title: 'Légende',
            okType: 'button-balanced',
            template: '<div id="POPIN_ICONES"><div class="uneligne_icones article"><a><span class="img_icones"><img src="images/picto_coeur.png"/></span><span class="txt_icones">Coup de coeur</span></a></div><div class="uneligne_icones article"><a><span class="img_icones"><img src="images/picto_redworld.png"/></span><span class="txt_icones">Succès international</span></a></div><div class="uneligne_icones article"><a><span class="img_icones"><img src="images/picto_redtower.png"/></span><span class="txt_icones">Succès parisien</span></a></div><div class="uneligne_icones article"><a><span class="img_icones"><img src="images/picto_redprix.png"/></span><span class="txt_icones">Primé</span></a></div><div class="uneligne_icones article"><a><span class="img_icones"><img src="images/picto_redpeople2.png"/></span><span class="txt_icones">Tout public</span></a></div><div class="uneligne_icones article"><a><span class="img_icones"><img src="images/picto_redpeople.png"/></span><span class="txt_icones">Déconseillé jeune public</span></a></div><div class="uneligne_icones article"><a><span class="img_icones"><img src="images/picto_redschool.png"/></span><span class="txt_icones">Matinées scolaires</span></a></div><div class="uneligne_icones article"><a><span class="img_icones"><img src="images/picto_redone.png"/></span><span class="txt_icones">Bambino</span></a></div></div>'
        });
    };

    myAsyncTasks = [];
    myAsyncTasks.push($scope.ready());
    $q.all(myAsyncTasks).then(function(results) {
        $scope.resolve = true;
    }, function(results) {});

})


.controller('ListeCtrl', function($scope, $q, $rootScope, WSService, $stateParams, $timeout, $ionicScrollDelegate, $location, localStorageService, $ionicPopup, $state,PGCons) {
    //$scope.$on('$ionicView.beforeEnter', function() {
    $scope.selparam = null;
    $scope.showWaiting();
    $scope.resolve = false;
    $scope.trackPage('listePage');
    $scope.geninput = {};
    $scope.mySelection = localStorageService.get("mySelection") || [];
    var currentPage = 1;
    var nbPageMax = 0;
    var critGen = null;
    var critSel = null;
    var critRech = null;
    var critDate = '';
    var critIdList = null;
    $scope.addItem = true;
    $scope.mode = $stateParams.mode || false;
    $lastMonth = false;
    $scope.genreList=PGCons.genreList;

    //A partir du 01/09/15 on calcule la date
    if (new Date().getTime() < 1441058460000) {
        critDate = '2015-09-01';
    } else {
        var d = new Date();
        critDate = d.getFullYear() + '-' + ("0" + (d.getMonth() + 1)).slice(-2) + '-' + ("0" + (d.getDate() + 1)).slice(-2);
    }
    $scope.moreDataCanBeLoaded = false;

    var deferred = $q.defer();
    $timeout(function() {
        deferred.resolve();
    }, 1e4);
    var myAsyncTasks = [];
    if ($stateParams.mode === 'search') {
        //recherche
        critRech = $rootScope.txtSearch;
        myAsyncTasks.push(WSService.getSpectacleList(null, null, null, null, null, null, critRech, deferred));
    } else if ($stateParams.mode === 'myselection') {
        critIdList = "";
        $scope.addItem = false;
        for (var i = 0; i < $scope.mySelection.length; i++) {
            if ($scope.mySelection[i]) {
                critIdList += i + ",";
            }
        }
        if (critIdList !== "") {
            //retrait de la dernière virgule
            critIdList = critIdList.slice(0, (critIdList.length) - 1);
            //ma selection
            myAsyncTasks.push(WSService.getSpectacleList(null, null, null, null, null, critIdList, null, deferred));
        }
    } else {
        //clic sur un mois
        myAsyncTasks.push(WSService.getSpectacleList(critDate, null, null, null, null, null, null, deferred));
    }
    myAsyncTasks.push($scope.ready());

    $scope.removeFromSelection = function(id, index) {
        var mySelection = localStorageService.get("mySelection");
        var confirmPopup = $ionicPopup.confirm({
            title: 'Ma sélection',
            template: '“Voulez vous retirer ce spectacle de votre sélection”',
            cancelText: 'Annuler',
            okType: 'button-balanced'
        });
        confirmPopup.then(function(res) {
            if (res) {
                mySelection[id] = false;
                localStorageService.set("mySelection", mySelection);
                $scope.WSData.remove(index);
                //1 = le premier mois
                if ($scope.WSData.length === 1) {
                    $scope.WSData = null;
                }
            }
        });
    };

    $scope.loadMoreData = function() {
        currentPage++;
        WSService.getSpectacleList(critDate, null, currentPage, critSel, critGen, critIdList, critRech).success(function(data) {
            if (currentPage > nbPageMax) {
                console.log('Debug : anormal');
                $scope.moreDataCanBeLoaded = false;
                currentPage--;
                return;
            }
            $scope.$broadcast('scroll.infiniteScrollComplete');
            //Retrait des mois affichés en doublons
            if (data.lstdata[0].id === 0) {
                if (lastMonth === data.lstdata[0].titre) {
                    console.log('slice');
                    data.lstdata = data.lstdata.slice(1);
                }
            }
            for (var i = 0; i < data.lstdata.length; i++) {
                if (data.lstdata[i].id === 0) {
                    lastMonth = data.lstdata[i].titre;
                }
            }
            $scope.WSData = $scope.WSData.concat(data.lstdata);
            console.log(currentPage + 'vs' + nbPageMax);
            if (currentPage === nbPageMax) {
                console.log('désactivation infinite');
                $scope.moreDataCanBeLoaded = false;
                $timeout(function() {
                    $ionicScrollDelegate.resize();
                }, 500);
            }
        }).
        error(function(data) {});
    };

    $scope.filterSel = function(selparam) {
        $scope.moreDataCanBeLoaded = false;
        currentPage = 1;
        $scope.showWaiting();
        // permet de distinguer le prochainement
        if (selparam !== null) {
            $scope.selparam = selparam.clef;
        } else {
            selparam = {};
            selparam.clef = null;
        }
        critSel = selparam.clef;
        WSService.getSpectacleList(critDate, null, null, critSel, critGen, null, null).success(function(data) {
            $scope.WSData = data.lstdata;
            nbPageMax = Math.ceil(data.countmax / 10);
            if (nbPageMax > 1) {
                $timeout(function() {
                    $scope.moreDataCanBeLoaded = true;
                }, 2000);
            }
            $scope.hideWaiting();
        }).
        error(function(data) {
            $scope.hideWaiting();
        });
    };
    $scope.filterGen = function(genparam) {
        $scope.moreDataCanBeLoaded = false;
        currentPage = 1;
        $scope.showWaiting();
        // permet de distinguer le tous les genres
        if (genparam !== null) {
            $scope.genparam = genparam.clef;
        } else {
            genparam = {};
            genparam.clef = null;
        }
        critGen = genparam.clef;
        WSService.getSpectacleList(critDate, null, null, critSel, critGen, null, null).success(function(data) {
            $scope.WSData = data.lstdata;
            nbPageMax = Math.ceil(data.countmax / 10);
            if (nbPageMax > 1) {
                $timeout(function() {
                    $scope.moreDataCanBeLoaded = true;
                }, 2000);
            }
            $scope.hideWaiting();
        }).
        error(function(data) {
            $scope.hideWaiting();
        });
    };
    $scope.filterDate = function(date) {
        $scope.showWaiting();
        //Désactivation infinite
        $scope.moreDataCanBeLoaded = false;
        $scope.date = date;
        critDate = date;
        currentPage = 1;
        WSService.getSpectacleList(critDate, null, null, critSel, critGen, null, null).success(function(data) {
            $scope.WSData = data.lstdata;
            nbPageMax = Math.ceil(data.countmax / 10);
            console.log('nbPageMax ' + nbPageMax);
            if (nbPageMax > 1) {
                $timeout(function() {
                    $scope.moreDataCanBeLoaded = true;
                }, 2000);
            }
            $scope.hideWaiting();
        }).
        error(function(data) {
            $scope.hideWaiting();
        });
    };
    $scope.date = critDate;

    $q.all(myAsyncTasks).then(function(results) {
        $scope.resolve = true;
        //pas de sélection, on hide
        if (critIdList === '') {
            $scope.hideWaiting();
        } else {
            if (results[0].data.count > 0) {
                //$scope.WSData = results[0].data.lstdata;
                $scope.WSData = results[0].data.lstdata;
                for (var i = 0; i < $scope.WSData.length; i++) {
                    if ($scope.WSData[i].id === 0) {
                        //calcul du dernier mois affiché
                        lastMonth = $scope.WSData[i].titre;
                    }
                }
                $timeout(function() {
                    $scope.hideWaiting();
                }, 500);
                //A 200 c'est moyen sur ip4
                if (results[0].data.countmax > 10) {
                    nbPageMax = Math.ceil(results[0].data.countmax / 10);
                    if (nbPageMax > 1) {
                        $timeout(function() {
                            $scope.moreDataCanBeLoaded = true;
                        }, 2000);
                    }
                }
            } else {
                //pas de résultat, on hide
                $scope.hideWaiting();
            }
        }
    }, function(results) {
        //if (results.status === 0) {
        $scope.hideWaiting();
        $state.go('app.timeout');
        //}
    });

    //});
    $scope.$on('$ionicView.beforeLeave', function() {
        if (typeof cordova !== "undefined") {
            cordova.plugins.Keyboard.hideKeyboardAccessoryBar(false);
        }
    });

})

.controller('SpectacleCtrl', function($scope, WSService, $stateParams) {
    $scope.trackPage('spectaclePage');
    $scope.openFiche($stateParams.idf, false);
})

.controller('AgendaCtrl', function($scope, WSService, $timeout, $ionicSlideBoxDelegate, $q, $ionicScrollDelegate, $state) {
    $scope.trackPage('agendaPage');
    $scope.showWaiting();
    $scope.resolve = false;
    $scope.updateFiche = function(day) {
        $scope.current = day;
        $timeout(function() {
            $ionicScrollDelegate.resize();
        }, 150);
    };

    $scope.nextSlide = function() {
        $ionicSlideBoxDelegate.$getByHandle('agendaBox').next();
    };
    $scope.previousSlide = function() {
        $ionicSlideBoxDelegate.$getByHandle('agendaBox').previous();
    };

    $scope.myrange = function(tab) {
        var range = [];
        for (var i = 0; i < tab.length; i = i + 7)
            range.push(i);
        return range;
    };
    var deferred = $q.defer();
    $timeout(function() {
        deferred.resolve();
    }, 1e4);
    var myAsyncTasks = [];
    myAsyncTasks.push(WSService.getAgenda(deferred));
    myAsyncTasks.push($scope.ready());

    $q.all(myAsyncTasks).then(function(results) {
        $scope.resolve = true;
        $scope.agenda = results[0].data;
        $timeout(function() {
            $scope.hideWaiting();
        }, 500);
    }, function(results) {
        //if (results.status === 0) {
        $scope.hideWaiting();
        $state.go('app.timeout');
        //}
    });
})

.controller('BilletsCtrl', function($scope, WSService, localStorageService, $ionicPopup, $q, $timeout) {
    $scope.trackPage('billetPage');
    $scope.showWaiting();

    if (!localStorageService.get("inCalList")) {
        localStorageService.set("inCalList", []);
    }
    var inCalList = localStorageService.get("inCalList");

    $scope.resolve = false;
    var deferred = $q.defer();
    $timeout(function() {
        deferred.resolve();
    }, 1e4);
    var myAsyncTasks = [];
    myAsyncTasks.push(WSService.loadCommande(deferred, localStorageService.get("login"), localStorageService.get("mdp"), localStorageService.get("idutil")));
    myAsyncTasks.push($scope.ready());

    $q.all(myAsyncTasks).then(function(results) {
        $scope.resolve = true;
        var data = results[0].data;
        localStorageService.set(localStorageService.get("idutil") + "_billets", data.billets);
        $scope.billets = data.billets;
        //Impact du calendrier
        if (localStorageService.get("saveInCal") || false) {
            if (window.plugins.calendar) {
                var eventLocation = "Pin Galant";
                var notes = "";
                var calOptions = window.plugins.calendar.getCalendarOptions(); // grab the defaults
                calOptions.firstReminderMinutes = 1440; // default is 60, pass in null for no reminder (alarm)
                calOptions.secondReminderMinutes = 120;
                var success = function(message) {
                    $ionicPopup.alert({
                        title: 'Calendrier',
                        okType: 'button-balanced',
                        template: 'Calendrier natif mis à jour.'
                    });
                };
                var error = function(message) {
                    alert("Erreur: " + message);
                };

                for (var i = 0; i < data.billets.length; i++) {
                    //vérif que l'entrée n'existe pas deja
                    if (!inCalList[data.billets[i].idsirius] /*&& (i === 1 || i === 2)*/ ) {
                        var startDate = new Date(data.billets[i].millisec); // beware: month 0 = january, 11 = december
                        var endDate = new Date(data.billets[i].millisec + 3600000);
                        var title = data.billets[i].titre;
                        inCalList[data.billets[i].idsirius] = true;
                        window.plugins.calendar.createEventWithOptions(title, eventLocation, notes, startDate, endDate, calOptions, success, error);
                    }
                }
                localStorageService.set("inCalList", inCalList);
            } else {
                console.log('window.plugins.calendar inconnu');
            }
        }
        $scope.hideWaiting();
    }, function(results) {
        console.log('Billets en cache');
        //if (results.status === 0) {
        $scope.hideWaiting();
        $scope.billets = localStorageService.get(localStorageService.get("idutil") + "_billets");
        //aller chercher les billets en cache
        //}
        $scope.resolve = true;
    });
})

.controller('BilletsListeCtrl', function($scope, localStorageService, $ionicNavBarDelegate, $ionicHistory, $stateParams) {
    $scope.trackPage('billetListePage');
    $scope.showWaiting();
    $ionicNavBarDelegate.showBackButton(false);
    $scope.goback = function() {
        $ionicHistory.goBack();
    };
    var billets = localStorageService.get(localStorageService.get("idutil") + "_billets");
    $scope.billet = billets[$stateParams.idx];
    localStorageService.set(localStorageService.get("idutil") + "_billet", $scope.billet);

    ionic.DomUtil.ready(function() {
        $scope.hideWaiting();
    });
})

.controller('BilletsDetailCtrl', function($scope, localStorageService, $ionicHistory, $ionicNavBarDelegate, $stateParams, $ionicSlideBoxDelegate, $timeout) {
    $scope.trackPage('billetDetailPage');
    $scope.showWaiting();
    $ionicNavBarDelegate.showBackButton(false);
    $scope.goback = function() {
        $ionicHistory.goBack();
    };
    $scope.nextSlide = function() {
        $ionicSlideBoxDelegate.$getByHandle('billetsBox').next();
    };
    $scope.previousSlide = function() {
        $ionicSlideBoxDelegate.$getByHandle('billetsBox').previous();
    };

    $scope.billet = localStorageService.get(localStorageService.get("idutil") + "_billet");
    $timeout(function() {
        $ionicSlideBoxDelegate.$getByHandle('billetsBox').slide($stateParams.idx);
        $ionicSlideBoxDelegate.$getByHandle('billetsBox').update();
    }, 500);

    ionic.DomUtil.ready(function() {
        $scope.hideWaiting();
    });
})

.controller('LoginCtrl', function($scope, WSService, $location, $ionicHistory, localStorageService, $ionicPopup, $q, $timeout, $state) {
    $scope.trackPage('loginPage');
    $scope.showWaiting();
    //$scope.loginData.username = "pmoreau@gmail.com";
    $ionicHistory.nextViewOptions({
        disableBack: true
    });
    $scope.doLogin = function() {
        var deferred = $q.defer();
        $timeout(function() {
            deferred.resolve();
        }, 1e4);
        var myAsyncTasks = [];
        myAsyncTasks.push(WSService.login(deferred, $scope.loginData.username, $scope.loginData.password));

        $q.all(myAsyncTasks).then(function(results) {
            console.log(results[0]);
            if (results[0].data.idutil || false) {
                localStorageService.set("login", $scope.loginData.username);
                localStorageService.set("mdp", $scope.loginData.password);
                localStorageService.set("idutil", results[0].data.idutil);
                $scope.updateLogin(true);
                //TODO optim redir
                $location.url("/app/billets");
            } else {
                $ionicPopup.alert({
                    title: 'Connexion',
                    okType: 'button-balanced',
                    template: 'Identifiant et/ou mot de passe incorrect'
                });
            }
        }, function(results) {
            //if (results.status === 0) {
            $scope.hideWaiting();
            $state.go('app.timeout');
            //}
        });
    };
    ionic.DomUtil.ready(function() {
        $scope.hideWaiting();
    });


})

.controller('LoginLostCtrl', function($scope, WSService, $location, $ionicPopup, $q, $timeout, $state, $ionicNavBarDelegate) {
    $scope.trackPage('loginLostPage');
    $scope.showWaiting();
    $ionicNavBarDelegate.showBackButton(false);

    //$scope.loginData.username = "pmoreau@gmail.com";

    $scope.doLogin = function() {
        var deferred = $q.defer();
        $timeout(function() {
            deferred.resolve();
        }, 1e4);
        var myAsyncTasks = [];
        myAsyncTasks.push(WSService.login(deferred, $scope.loginData.username));

        $q.all(myAsyncTasks).then(function(results) {
            $ionicPopup.alert({
                title: 'Mot de passe',
                okType: 'button-balanced',
                template: 'Votre mot de passe vient de d\'être envoyé à l\'adresse mail que vous avez indiqué.'
            });
        }, function(results) {
            //if (results.status === 0) {
            $scope.hideWaiting();
            $state.go('app.timeout');
            //}
        });
    };
    ionic.DomUtil.ready(function() {
        $scope.hideWaiting();
    });

})

.controller('LoginCmdCtrl', function($scope, WSService, $location, $ionicPopup, $q, $timeout, $state, $ionicNavBarDelegate) {
    $scope.trackPage('loginCmdPage');
    $scope.showWaiting();
    $ionicNavBarDelegate.showBackButton(false);

    //$scope.loginData.username = "pmoreau@gmail.com";

    $scope.doLogin = function() {
        var deferred = $q.defer();
        $timeout(function() {
            deferred.resolve();
        }, 1e4);
        var myAsyncTasks = [];
        myAsyncTasks.push(WSService.login(deferred, $scope.loginData.username, false, $scope.loginData.cmd));

        $q.all(myAsyncTasks).then(function(results) {
            /*$ionicPopup.alert({
                title: 'Mot de passe',
                okType: 'button-balanced',
                template: 'Votre mot de passe vient de d\'être envoyé à l\'adresse mail que vous avez indiqué.'
            });*/
        }, function(results) {
            //if (results.status === 0) {
            $scope.hideWaiting();
            $state.go('app.timeout');
            //}
        });
    };
    ionic.DomUtil.ready(function() {
        $scope.hideWaiting();
    });

})

.controller('ActuCtrl', function($scope, WSService, $q, $timeout, $state) {
    $scope.trackPage('actuPage');
    $scope.showWaiting();

    $scope.resolve = false;
    var deferred = $q.defer();
    $timeout(function() {
        deferred.resolve();
    }, 1e4);
    var myAsyncTasks = [];
    myAsyncTasks.push(WSService.loadActualite(deferred));
    myAsyncTasks.push($scope.ready());

    $q.all(myAsyncTasks).then(function(results) {
        $scope.resolve = true;
        $scope.actualites = results[0].data.lstinfo;
        $scope.hideWaiting();
    }, function(results) {
        //if (results.status === 0) {
        $scope.hideWaiting();
        $state.go('app.timeout');
        //}
    });

})

.controller('ParamCtrl', function($scope, localStorageService) {
    $scope.trackPage('ParamPage');
    $scope.saveInCal = {};
    $scope.saveInCal.checked = localStorageService.get("saveInCal") || false;
    $scope.onToogle = function(val) {
        localStorageService.set("saveInCal", val);
    };
})

.controller('LoadingCtrl', function($scope, $ionicLoading, $location, PGCons, $stateParams) {
    goInApp = function(url) {
        var ref = window.open(url, '_blank', 'location=no,closebuttoncaption=Quitter la réservation');
    };
    goOutApp = function(url) {
        var ref = window.open(url, '_system');
    };

    if ($location.path().indexOf(PGCons.inAppUrl) > -1) {
        goInApp($stateParams.url);
        $location.url('/app/liste/');
    } else if ($location.path().indexOf(PGCons.extUrl) > -1) {
        goOutApp($stateParams.url);
        $location.url('/app/liste/');
    }

    $scope.$on('$ionicView.beforeLeave', function() {
        //$ionicLoading.hide();
    });
})

.controller('RemoteCtrl', function($scope, WSService, $sce, $stateParams, $q, $timeout, $state) {
    $scope.trackPage('remotePage_' + $stateParams.IDP);
    $scope.showWaiting();

    var deferred = $q.defer();
    $timeout(function() {
        deferred.resolve();
    }, 1e4);
    var myAsyncTasks = [];
    myAsyncTasks.push(WSService.getRemotePage(deferred, $stateParams.IDP));
    myAsyncTasks.push($scope.ready());

    $q.all(myAsyncTasks).then(function(results) {
        $scope.htmlCode = $sce.trustAsHtml(results[0].data.texte);
        $scope.titre = results[0].data.titre;
        $scope.hideWaiting();
    }, function(results) {
        //if (results.status === 0) {
        $scope.hideWaiting();
        $state.go('app.timeout');
        //}
    });
})

.controller('ResaCtrl', function($scope, WSService, $q, $timeout, $state) {
    $scope.trackPage('resaPage');
    $scope.showWaiting();

    $scope.resolve = false;
    if ($scope.saisonouverte === 1) {
        var deferred = $q.defer();
        $timeout(function() {
            deferred.resolve();
        }, 1e4);
        var myAsyncTasks = [];
        myAsyncTasks.push(WSService.loadReservation(deferred));
        myAsyncTasks.push($scope.ready());

        $q.all(myAsyncTasks).then(function(results) {
            $scope.resolve = true;
            $scope.specList = results[0].data.spectacles;
            $scope.hideWaiting();
        }, function(results) {
            //if (results.status === 0) {
            $scope.hideWaiting();
            $state.go('app.timeout');
            //}
        });
    } else {
        $scope.resolve = true;
        $scope.hideWaiting();
    }
})

.controller('ParamCtrl', function($scope, localStorageService) {
    $scope.trackPage('ParamPage');
    $scope.saveInCal = {};
    $scope.saveInCal.checked = (localStorageService.get("saveInCal") === 'true');
    $scope.onToogle = function(val) {
        localStorageService.set("saveInCal", val);
    };
})


.controller('TimeoutCtrl', function($scope, $ionicNavBarDelegate, $sce) {
    $ionicNavBarDelegate.showBackButton(false);
});
