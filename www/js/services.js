angular.module('PGapp.services', [])
    .factory('WSService', ['$http',
        function($http) {
            var server = "http://www.lepingalant.com";
            var getAgenda = function(deferred) {
                //return $http.get('json/agenda2.json');
                var url = server + "/load_calendrier.php?callback=JSON_CALLBACK" + "&platform=" + ionic.Platform.platform() + "&uuid=" + window.device.uuid;
                return $http({
                    method: 'JSONP',
                    url: url,
                    timeout: deferred.promise
                });
            };
            var getRemotePage = function(deferred, i) {
                alert('&uuid=' + window.device.uuid);
                var url = server + "/load_page.php?IDP=" + i + "&callback=JSON_CALLBACK" + "&platform=" + ionic.Platform.platform() + "&uuid=" + window.device.uuid;
                return $http({
                    method: 'JSONP',
                    url: url,
                    timeout: deferred.promise
                });
            };
            var getLoadAppli = function(deferred) {
                //return $http.get('json/loadAppli.json');
                var url = server + "/load_appli.php?callback=JSON_CALLBACK" + "&platform=" + ionic.Platform.platform() + "&uuid=" + window.device.uuid;
                return $http({
                    method: 'JSONP',
                    url: url,
                    timeout: deferred.promise
                });
            };
            var getSpectacleList = function(dat, nbfp, pgc, sel, gen, lstidf, rech, deferred) {
                var param = "";
                if ((dat || '') !== "") {
                    param += "&DAT=" + dat;
                }
                if ((nbfp || '') !== "") {
                    param += "&NBFP=" + nbfp;
                }
                if ((pgc || '') !== "") {
                    param += "&PGC=" + pgc;
                }
                if ((sel || '') !== "") {
                    param += "&SEL=" + sel;
                }
                if ((gen || '') !== "") {
                    param += "&GENR=" + gen;
                }
                if ((lstidf || '') !== "") {
                    param += "&LSTIDF=" + lstidf;
                }
                if ((rech || '') !== "") {
                    param += "&RECH=" + encodeURIComponent(rech);
                }
                if (!deferred) {
                    deferred = {};
                    deferred.promise = {};
                }
                //alert('&uuid=' + window.device.uuid);
                var url = server + "/load_spectacle.php?callback=JSON_CALLBACK" + param + "&platform=" + ionic.Platform.platform() + "&uuid=" + window.device.uuid;
                return $http({
                    method: 'JSONP',
                    url: url,
                    timeout: deferred.promise
                });
            };
            var getSpectacle = function(deferred, idf) {
                var url = server + "/load_fiche.php?callback=JSON_CALLBACK&IDF=" + idf + "&platform=" + ionic.Platform.platform() + "&uuid=" + window.device.uuid;
                return $http({
                    method: 'JSONP',
                    url: url,
                    timeout: deferred.promise
                });
            };
            var login = function(deferred, em, mp, cm) {
                var param = "";
                if ((em || '') !== "") {
                    param += "&EM=" + em;
                }
                if ((mp || '') !== "") {
                    param += "&MDP=" + mp;
                }
                if ((cm || '') !== "") {
                    param += "&CM=" + cm;
                }
                var url = server + "/connect.php?callback=JSON_CALLBACK" + param + "&platform=" + ionic.Platform.platform() + "&uuid=" + window.device.uuid;
                return $http({
                    method: 'JSONP',
                    url: url,
                    timeout: deferred.promise
                });
            };
            var loadCommande = function(deferred, em, mp, cm) {
                var url = server + "/load_commande.php?callback=JSON_CALLBACK&EM=" + em + "&MDP=" + mp + "&CM=" + cm + "&platform=" + ionic.Platform.platform() + "&uuid=" + window.device.uuid;
                return $http({
                    method: 'JSONP',
                    url: url,
                    timeout: deferred.promise
                });
            };
            var loadActualite = function(deferred) {
                var url = server + "/load_actualite.php?callback=JSON_CALLBACK" + "&platform=" + ionic.Platform.platform() + "&uuid=" + window.device.uuid;
                return $http({
                    method: 'JSONP',
                    url: url,
                    timeout: deferred.promise
                });
            };
            var registerUser = function(uuid, token) {
                var url = "http://www.ouatoodoo.com/pingalant/registerapppg.php?callback=JSON_CALLBACK&token=" + token + "&id=" + uuid + "&platform=" + ionic.Platform.platform() + "&uuid=" + window.device.uuid;
                return $http({
                    method: 'JSONP',
                    url: url
                });
            };
            var loadReservation = function(deferred) {
                var url = server + "/load_reservation.php?callback=JSON_CALLBACK" + "&platform=" + ionic.Platform.platform() + "&uuid=" + window.device.uuid;
                return $http({
                    method: 'JSONP',
                    url: url,
                    timeout: deferred.promise
                });
            };
            return {
                getAgenda: function(deferred) {
                    return getAgenda(deferred);
                },
                getRemotePage: function(deferred, i) {
                    return getRemotePage(deferred, i);
                },
                getLoadAppli: function(deferred) {
                    return getLoadAppli(deferred);
                },
                getSpectacleList: function(dat, nbfp, pgc, sel, gen, idlist, rech, deferred) {
                    return getSpectacleList(dat, nbfp, pgc, sel, gen, idlist, rech, deferred);
                },
                getSpectacle: function(deferred, idf) {
                    return getSpectacle(deferred, idf);
                },
                login: function(deferred, em, mp, cm) {
                    return login(deferred, em, mp, cm);
                },
                loadCommande: function(deferred, em, mp, cm) {
                    return loadCommande(deferred, em, mp, cm);
                },
                loadActualite: function(deferred) {
                    return loadActualite(deferred);
                },
                registerUser: function(uuid, token) {
                    return registerUser(uuid, token);
                },
                loadReservation: function(deferred) {
                    return loadReservation(deferred);
                }
            };
        }
    ]);
