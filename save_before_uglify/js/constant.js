angular.module('PGapp.constants', [])
    .constant("PGCons", {
        "version": "0.0.0.1",
        "extUrl": "ext",
        "inAppUrl": "inapp",
        "interval": 8000,
        "proch": {
            lib: "Prochainement",
            version: "0.0.0.1",
            clef: ""
        },
        "genreList": [{
            clef: "",
            lib: "TOUS LES GENRES"
        }, {
            clef: "1",
            lib: "ARTS VISUELS"
        }, {
            clef: "2",
            lib: "BLUES"
        }, {
            clef: "3",
            lib: "CABARET"
        }, {
            clef: "4",
            lib: "CIRQUE"
        }, {
            clef: "5",
            lib: "CIRQUE MUSICAL"
        }, {
            clef: "6",
            lib: "CLOWNERIES"
        }, {
            clef: "7",
            lib: "COMÉDIE"
        }, {
            clef: "8",
            lib: "CONCERT"
        }, {
            clef: "28",
            lib: "COMÉDIE MUSICALE"
        }, {
            clef: "9",
            lib: "CONTE MUSICAL"
        }, {
            clef: "10",
            lib: "DANSE"
        }, {
            clef: "11",
            lib: "HUMOUR"
        }, {
            clef: "12",
            lib: "HUMOUR MUSICAL"
        }, {
            clef: "27",
            lib: "JAZZ"
        }, {
            clef: "13",
            lib: "MAGIE"
        }, {
            clef: "14",
            lib: "MAGIE / HUMOUR"
        }, {
            clef: "15",
            lib: "MARIONNETTES"
        }, {
            clef: "16",
            lib: "MUSIQUE ET RECYCLAGE"
        }, {
            clef: "17",
            lib: "OPÉRA"
        }, {
            clef: "18",
            lib: "OPÉRETTE"
        }, {
            clef: "19",
            lib: "SPECTACLE SANS PAROLES"
        }, {
            clef: "20",
            lib: "THÉÂTRE"
        }, {
            clef: "21",
            lib: "THÉÂTRE CLASSIQUE"
        }, {
            clef: "22",
            lib: "THÉÂTRE D’OBJETS"
        }, {
            clef: "23",
            lib: "THÉÂTRE HISTORIQUE"
        }, {
            clef: "24",
            lib: "THÉÂTRE MASQUÉ"
        }, {
            clef: "25",
            lib: "THÉÂTRE MUSICAL"
        }, {
            clef: "26",
            lib: "VARIÉTÉ"
        }]
    });
