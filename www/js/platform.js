window.plugins = window.plugins || {};
window.device = window.device || {};
window.platform = window.platform || {};
window.device.uuid = window.device.uuid || "PC100";
window.defaultBrightness=-1;
var gaPlugin = null;

Array.prototype.remove = function(from, to) {
  var rest = this.slice((to || from) + 1 || this.length);
  this.length = from < 0 ? this.length + from : from;
  return this.push.apply(this, rest);
};