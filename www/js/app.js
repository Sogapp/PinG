// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
// 'starter.controllers' is found in controllers.js
angular.module('PGapp', ['ionic', 'ngIOS9UIWebViewPatch', 'PGapp.controllers', 'monospaced.qrcode', 'LocalStorageModule', 'PGapp.services', 'PGapp.constants'])

.run(function($ionicPlatform, $state, $ionicLoading, $location, localStorageService, WSService) {
    $ionicLoading.show({
        template: '<ion-spinner icon="android" class="spinner-balanced"></ion-spinner><br>Chargement...'
    });

    localStorageService.set("lastAccess", new Date().getTime());
    $ionicPlatform.ready(function() {
        if (typeof StatusBar !== "undefined") {
            StatusBar.overlaysWebView(true);
            StatusBar.styleLightContent();
        }
        window.device.uuid = device.uuid || "PC100";
        setTimeout(function() {
            myOnDeviceReady(window.device.uuid);
            if (typeof navigator.splashscreen !== "undefined") {
                navigator.splashscreen.hide();
            }
        }, 200);

        // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
        // for form inputs)
        /*if (window.cordova && window.cordova.plugins.Keyboard) {
          cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
        }
        if (window.StatusBar) {
          // org.apache.cordova.statusbar required
          window.StatusBar.overlaysWebView(true);
          window.StatusBar.styleLightContent();
        }*/
        /*$ionicPlatform.onHardwareBackButton(function() {
                    if ($location.$$path == '/tab/home') {
                        ionic.Platform.exitApp();
                    }
                })*/
        //ionicPlatform = ionic.Platform;

        /*if (typeof window.StatusBar !== "undefined"){
          window.StatusBar.overlaysWebView(true);
          window.StatusBar.styleLightContent();
        }*/

        /* if (ionicPlatform.isIOS()) {
           alert('2');
             /*setTimeout(function() {
                 navigator.splashscreen.hide();
             }, 100);*/
        /*if (window.StatusBar) {
              alert('3');
                window.StatusBar.overlaysWebView(true);
                window.StatusBar.styleLightContent();
            }
        }*/
        /*if (ionicPlatform.isWindowsPhone()) {
            platform = "wp";
        } else if (ionicPlatform.isAndroid()) {
            platform = "android";
        } else if (ionicPlatform.isIOS()) {
            platform = "ios";
        } else {
          platform = "PC";
        }
        $rootScope.platform=platform;
        console.log(platform);*/

        function successHandler(result) {}

        function errorHandler(error) {}

        function tokenHandler(result) {
            isTokenRegistered = localStorageService.get('isTokenRegistered') || false;
            if (!isTokenRegistered && window.device.uuid) {
                WSService.registerUser(window.device.uuid, result).
                success(function(data) {
                    localStorageService.set('isTokenRegistered', true);
                    //alert('OK : ' + result);
                }).
                error(function(data) {
                    alert("Erreur, merci de réessayer plus tard." + JSON.stringify(data));
                    console.log('ERROR ' + data);
                });
            }
        }

        if (typeof window.plugins.gaPlugin !== "undefined") {
            gaPlugin = window.plugins.gaPlugin;
            //var successHandler = function(result) {};
            //var errorHandler = function(result) {};
            //gaPlugin.init(successHandler, errorHandler, "UA-44004652-1", 10);
            gaPlugin.init(successHandler, errorHandler, "UA-63270605-1", 10);
        }
        var pushNotification = null;
        if (typeof window.plugins.pushNotification !== "undefined") {

            pushNotification = window.plugins.pushNotification;

            if (ionic.Platform.isAndroid()) {
                pushNotification.register(
                    successHandler,
                    errorHandler, {
                        "senderID": "819796578572",
                        "ecb": "onNotification"
                    });
            } else if (ionic.Platform.isIOS()) {
                pushNotification.register(
                    tokenHandler,
                    errorHandler, {
                        "badge": "true",
                        "sound": "true",
                        "alert": "true",
                        "ecb": "onNotificationAPN"
                    });
            }

        }

        // Android and Amazon Fire OS
        onNotification = function(e) {
            switch (e.event) {
                case 'registered':
                    {
                        if (e.regid.length > 0) {
                            isTokenRegistered = localStorageService.get('isTokenRegistered') || false;
                            if (!isTokenRegistered && window.device.uuid) {
                                WSService.registerUser(window.device.uuid, e.regid).
                                success(function(data) {
                                    localStorageService.set('isTokenRegistered', true);
                                }).
                                error(function(data) {
                                    alert("Erreur, merci de réessayer plus tard." + JSON.stringify(data));
                                    console.log('ERROR ' + data);
                                });
                            }
                        }
                        break;
                    }
                case 'message':
                    {
                        var lastAccess = localStorageService.get("lastAccess");
                        //Si l'appli est lancée depuis plus de 10s alors on route
                        if (new Date().getTime() > lastAccess + 10000) {
                            spectacleID = e.payload.spectacleID || false;
                            pageID = e.payload.pageID || false;
                            if (spectacleID) {
                                $state.go('app.spectacle', {
                                    'idf': spectacleID
                                }, {
                                    reload: true
                                });
                            }
                            if (pageID) {
                                $state.go('app.remotePage', {
                                    'IDP': pageID
                                }, {
                                    reload: true
                                });
                            }
                        }
                        if (e.foreground) {
                            var soundfile = e.soundname || e.payload.sound;
                            var my_media = new Media("/android_asset/www/" + soundfile);
                            my_media.play();
                        } else {
                            if (e.coldstart) {} else {}
                        }
                        break;
                    }
                case 'error':
                    {
                        break;
                    }
                default:
                    {
                        break;
                    }
            }
        }

        onNotificationAPN = function(event) {
            var lastAccess = localStorageService.get("lastAccess");
            //Si l'appli est lancée depuis plus de 10s alors on route
            if (new Date().getTime() > lastAccess + 10000) {
                pageID = event.pageID || false;
                spectacleID = event.spectacleID || false;
                if (spectacleID) {
                    $state.go('app.spectacle', {
                        'idf': spectacleID
                    }, {
                        reload: true
                    });
                } else if (pageID) {
                    $state.go('app.remotePage', {
                        'IDP': pageID
                    }, {
                        reload: true
                    });
                }
            }
            if (event.alert) {
                navigator.notification.alert(event.alert);
            }
            if (event.sound) {
                var snd = new Media(event.sound);
                snd.play();
            }
            if (event.badge) {
                pushNotification.setApplicationIconBadgeNumber(successHandler, errorHandler, event.badge);
            }
        };
        console.log('path : ' + $location.$$path);
        //accès via URL par défaut
        if ($location.$$path === '') {
            $state.go('splash');
        }
        //}, 4000);
    });
})

.config(function($stateProvider, $urlRouterProvider, $ionicConfigProvider, $sceDelegateProvider) {
    $ionicConfigProvider.views.transition('none');
    $sceDelegateProvider.resourceUrlWhitelist(['self', new RegExp('^(http[s]?):\/\/(w{3}.)?youtube\.com/.+$')]);
    $sceDelegateProvider.resourceUrlWhitelist(['self', new RegExp('^(http[s]?):\/\/(w{3}.)?dailymotion\.com/.+$')]);
    $stateProvider
        .state('splash', {
            url: "/splash",
            cache: false,
            templateUrl: "templates/splash.html",
            controller: "SplashCtrl"
        })
        .state('loading', {
            url: "/loading",
            cache: false,
            controller: "LoadingCtrl"
        })

    .state('app', {
        url: "/app",
        abstract: true,
        templateUrl: "templates/menu.html",
        controller: 'AppCtrl'
    })

    .state('app.liste', {
            url: "/liste/:mode?",
            cache: false,
            views: {
                'menuContent': {
                    templateUrl: "templates/liste.html",
                    controller: 'ListeCtrl'
                }
            }
        })
        .state('ext', {
            url: "/ext?url",
            controller: "LoadingCtrl"
        })
        .state('inapp', {
            url: "/inapp?url",
            controller: "LoadingCtrl"
        })
        .state('app.spectacle', {
            url: "/spectacle/:idf",
            views: {
                'menuContent': {
                    templateUrl: "templates/spectacle.html",
                    controller: "SpectacleCtrl"
                }
            }
        })
        .state('app.billets', {
            url: "/billets",
            cache: false,
            views: {
                'menuContent': {
                    templateUrl: "templates/billets.html",
                    controller: 'BilletsCtrl'
                }
            }
        })
        .state('app.billetsListe', {
            url: "/billetsListe/:idx",
            cache: false,
            views: {
                'menuContent': {
                    templateUrl: "templates/billetsListe.html",
                    controller: 'BilletsListeCtrl'
                }
            }
        })
        .state('app.billetsDetail', {
            url: "/billetsDetail/:idx",
            cache: false,
            views: {
                'menuContent': {
                    templateUrl: "templates/billetsDetail.html",
                    controller: 'BilletsDetailCtrl'
                }
            }
        })
        .state('app.actu', {
            url: "/actu",
            cache: false,
            views: {
                'menuContent': {
                    templateUrl: "templates/actu.html",
                    controller: "ActuCtrl"
                }
            }
        })
        .state('app.agenda', {
            url: "/agenda",
            views: {
                'menuContent': {
                    templateUrl: "templates/agenda.html",
                    controller: "AgendaCtrl"
                }
            }
        })
        .state('app.beforeLogin', {
            url: "/beforeLogin",
            views: {
                'menuContent': {
                    templateUrl: "templates/beforeLogin.html"
                }
            }
        })
        .state('app.login', {
            url: "/login",
            views: {
                'menuContent': {
                    templateUrl: "templates/login.html",
                    controller: "LoginCtrl"
                }
            }
        })
        .state('app.loginLost', {
            url: "/loginLost",
            views: {
                'menuContent': {
                    templateUrl: "templates/loginLost.html",
                    controller: "LoginLostCtrl"
                }
            }
        })
        .state('app.loginCmd', {
            url: "/loginCmd",
            views: {
                'menuContent': {
                    templateUrl: "templates/loginCmd.html",
                    controller: "LoginCmdCtrl"
                }
            }
        })
        .state('app.param', {
            url: "/param",
            views: {
                'menuContent': {
                    templateUrl: "templates/param.html",
                    controller: "ParamCtrl"
                }
            }
        })
        .state('app.remotePage', {
            url: "/remotePage/:IDP",
            cache: false,
            views: {
                'menuContent': {
                    templateUrl: "templates/remotePage.html",
                    controller: "RemoteCtrl"
                }
            }
        })
        .state('app.resa', {
            url: "/resa",
            cache: false,
            views: {
                'menuContent': {
                    templateUrl: "templates/quickResa.html",
                    controller: "ResaCtrl"
                }
            }
        })
        .state('app.timeout', {
            url: "/timeout",
            views: {
                'menuContent': {
                    templateUrl: "templates/timeout.html",
                    controller: "TimeoutCtrl"
                }
            }
        });
    // if none of the above states are matched, use this as the fallback
    //$urlRouterProvider.otherwise('/app/liste/'+("0" + ((new Date()).getMonth() + 1)).slice(-2));
    //$urlRouterProvider.otherwise('/splash');
});
